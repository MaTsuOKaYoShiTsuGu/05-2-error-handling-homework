package com.twuc.webApp.domain.mazeRender.cellRenders;

import com.twuc.webApp.domain.mazeGenerator.GridCell;
import com.twuc.webApp.domain.mazeRender.CellRender;
import com.twuc.webApp.domain.mazeRender.RenderCell;
import com.twuc.webApp.domain.mazeRender.RenderType;

import java.awt.*;

/**
 * 该渲染器使用 Tag 中的整数数据对迷宫的网格进行渐变渲染。
 */
public class TagValueGradientRender implements CellRender {
    private final String tagKey;
    private final int min;
    private final int max;
    private final Color gradientStart;
    private final Color gradientEnd;
    private final float stepR;
    private final float stepG;
    private final float stepB;

    /**
     * 创建一个 {@link TagValueGradientRender} 实例。
     *
     * @param tagKey 整数 tag 数据的 key。该 key 将用于从 {@link GridCell#getTags()} 中提取数据。
     * @param min 代表渐变的最小值的整数。
     * @param max 代表渐变的最大值的整数。
     * @param gradientStart 渐变的起始颜色。
     * @param gradientEnd 渐变的终止颜色。
     */
    public TagValueGradientRender(String tagKey, int min, int max, Color gradientStart, Color gradientEnd) {
        this.tagKey = tagKey;
        this.min = min;
        this.max = max;
        this.gradientStart = gradientStart;
        this.gradientEnd = gradientEnd;
        stepR = ((float)gradientEnd.getRed() - gradientStart.getRed()) / (max - min);
        stepG = ((float)gradientEnd.getGreen() - gradientStart.getGreen()) / (max - min);
        stepB = ((float)gradientEnd.getBlue() - gradientStart.getBlue()) / (max - min);
    }

    @Override
    public void render(Graphics context, Rectangle cellArea, RenderCell cell) {
        Color renderingColor = getRenderingColor(cell);
        if (renderingColor == null) return;

        context.setColor(renderingColor);
        context.fillRect(cellArea.x, cellArea.y, cellArea.width, cellArea.height);
    }

    private Color getRenderingColor(RenderCell cell) {
        Object tag = cell.getTag(tagKey);
        if (tag == null)
        {
            RenderCell neighborGround = cell.getNeighbors().stream()
                .filter(n -> n.getRenderType() == RenderType.GROUND)
                .findFirst().orElse(null);
            return neighborGround != null ? getRenderingColor(neighborGround) : null;
        }

        if (!(tag instanceof Integer)) return null;
        int value = (int)tag;

        Color theColor;

        if (value <= min) theColor = gradientStart;
        else if (value >= max) theColor = gradientEnd;
        else
        {
            int diff = (value - min);
            theColor = new Color(
                (int)(gradientStart.getRed() + diff * stepR),
                (int)(gradientStart.getGreen() + diff * stepG),
                (int)(gradientStart.getBlue() + diff * stepB));
        }

        return theColor;
    }
}
